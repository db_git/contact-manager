/*
 *	ZavrsniProjekt
 *	Copyright (C) 2020  Denis Bošnjaković
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if defined(_MSC_VER)		\
 || defined(_MSC_FULL_VER)	\
 || defined(_MSC_BUILD)
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE
#endif /* _MSC_VER, _MSC_FULL_VER, _MSC_BUILD */

#include <cmakeconf.h>
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef HAS_WCHAR_H
#include <wctype.h>
#include <wchar.h>
#else
#include <ctype.h>
#include <string.h>
#endif /* HAS_WCHAR_H */

#include "definitions/bool.h"
#include "definitions/int.h"
#include "definitions/restrict.h"
#include "definitions/struct.h"
#include "definitions/wide.h"
#include "functions/input.h"
#include "functions/file.h"
#include "functions/memory.h"
#include "messages/file.h"
#include "messages/print.h"

void
check_empty_file(bool_t * restrict_ptr const is_empty)
{
	int_u32_t broj = 0;
	FILE * restrict_ptr const kontakti_fp = fopen(
	DATA_DIR "/kontakti.bin", "rb");

	if (!kontakti_fp || errno == ENOENT) {
		*is_empty = TRUE_DEF;
	} else {
		if (!fread(&broj, sizeof broj, 1, kontakti_fp))
			*is_empty = TRUE_DEF;
		else
			*is_empty = FALSE_DEF;
		fclose(kontakti_fp);
	}

	errno = 0;
	return;
}

/* Load contacts from file */
struct Kontakt **
ucitaj_kontakte(int_u32_t * restrict_ptr const broj_kontakata)
{
	register int_u32_t i = 0;
	register FILE *kontakti_fp = fopen(DATA_DIR "/kontakti.bin", "rb");
	register struct Kontakt ** restrict_ptr polje_kontakti = NULL;

	if (!kontakti_fp || errno == ENOENT) {
		PRINT_ERRNO(ENOENT);
		PRINT_ERR(PR_STR, WIDE(MSG_ERR_FILE_READ));
		exit(-ENOENT);
	} else if(!fread(broj_kontakata,
	sizeof *broj_kontakata, 1, kontakti_fp)) {
		PRINT_ERRNO(EIO);
		PRINT_ERR(PR_STR, WIDE(MSG_ERR_FILE_FREAD));
		exit(-EIO);
	} else {
		polje_kontakti = struct_alloc(*broj_kontakata);
	}

	for (i = 0; i < *broj_kontakata; ++i)
		if(!fread(*(polje_kontakti + i),
		sizeof **polje_kontakti, 1, kontakti_fp)) {
			PRINT_ERRNO(EIO);
			PRINT_ERR(PR_STR, WIDE(MSG_ERR_FILE_FREAD));
			exit(-EIO);
	}

	fclose(kontakti_fp);
	return polje_kontakti;
}

/* Save contacts to file */
void
spremi_kontakte(
	struct Kontakt * restrict_ptr const * restrict_ptr const polje_kontakti,
	const int_u32_t broj_kontakata)
{
	register int_u32_t i = 0;
	FILE * restrict_ptr const kontakti_fp = fopen(
	DATA_DIR "/kontakti.bin", "wb");

	if (!kontakti_fp || errno == ENOENT) {
		PRINT_ERRNO(ENOENT);
		PRINT_ERR(PR_STR, WIDE(MSG_ERR_FILE_WRITE));
		exit(-ENOENT);
	} else if (!fwrite(&broj_kontakata,
		sizeof broj_kontakata, 1, kontakti_fp)) {
		PRINT_ERRNO(EIO);
		PRINT_ERR(PR_STR, WIDE(MSG_ERR_FILE_FWRITE));
		exit(-EIO);
	}

	for (i = 0; i < broj_kontakata; ++i) {
		if (!fwrite(*(polje_kontakti + i),
		sizeof **polje_kontakti, 1, kontakti_fp)) {
			PRINT_ERRNO(EIO);
			PRINT_ERR(PR_STR, WIDE(MSG_ERR_FILE_FWRITE));
			exit(-EIO);
		}
	}

	fclose(kontakti_fp);
	return;
}
