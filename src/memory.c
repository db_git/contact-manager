/*
 *	ZavrsniProjekt
 *	Copyright (C) 2020  Denis Bošnjaković
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmakeconf.h>
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "definitions/int.h"
#include "definitions/restrict.h"
#include "definitions/wide.h"
#include "functions/memory.h"
#include "messages/memory.h"
#include "messages/print.h"

/* Safer version of calloc. Terminates when allocation fails */
void *
scalloc(const int_u32_t elements, const int_u32_t size)
{
	void *alloc_ptr = calloc(elements, size);
	if(!alloc_ptr || errno == ENOMEM) {
		PRINT_ERRNO(ENOMEM);
		PRINT_ERR(MSG_ERR_CALLOC, elements, size);
		exit(-ENOMEM);
	}

	return alloc_ptr;
}

/* Delete data and free memory */
void
sfree(void * restrict_ptr * restrict_ptr alloc_ptr,
			const int_u32_t elements,
			const int_u32_t size)
{
	memzero(*alloc_ptr, elements, size);
	free(*alloc_ptr);
	*alloc_ptr = NULL;
	return;
}

/* Safer version of realloc. Terminates when reallocation fails */
void *
srealloc(void *alloc_ptr, const int_u32_t old_el,
	const int_u32_t new_el, const int_u32_t size)
{
	void *tmp_ptr = realloc(alloc_ptr, size*(old_el + new_el));
	if (!tmp_ptr || errno == ENOMEM) {
		PRINT_ERRNO(ENOMEM);
		PRINT_ERR(PR_STR, WIDE(MSG_ERR_REALLOC));
		exit(-ENOMEM);
	}

	/* If expanding array initialise memory to zero */
	if (new_el > old_el)
		memzero((char *) tmp_ptr + old_el*size, new_el, size);

	return tmp_ptr;
}
