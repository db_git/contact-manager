/*
 *	ZavrsniProjekt
 *	Copyright (C) 2020  Denis Bošnjaković
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if defined(_MSC_VER)		\
 || defined(_MSC_FULL_VER)	\
 || defined(_MSC_BUILD)
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE
#endif /* _MSC_VER, _MSC_FULL_VER, _MSC_BUILD */

#include <cmakeconf.h>
#include <errno.h>
#include <stddef.h>
#include <stdio.h>

#ifdef HAS_WCHAR_H
#include <stdlib.h>
#include <wchar.h>
#endif /* HAS_WCHAR_H */

#include "definitions/inline.h"
#include "definitions/int.h"
#include "definitions/restrict.h"
#include "definitions/struct.h"
#include "functions/file.h"
#include "functions/vcard.h"
#include "messages/print.h"
#include "messages/vcard.h"

#ifdef HAS_WCHAR_H

inline_func static int_u8_t ucp_to_utf8(int_u8_t * restrict_ptr, const wchar_t);
inline_func static void write_utf8(const wchar_t * restrict_ptr const,
					FILE * restrict_ptr const);

/*
 * start       end        1st byte    2nd byte    3rd byte    4th byte
 * U+0000     U+7F        0xxxxxxx     <none>      <none>      <none>
 * U+0080     U+7FF       110xxxxx    10xxxxxx     <none>      <none>
 * U+0800     U+FFFF      1110xxxx    10xxxxxx    10xxxxxx     <none>
 * U+10000    U+10FFFF    11110xxx    10xxxxxx    10xxxxxx    10xxxxxx
 *
 * 110xxxxx = 0xC0
 * 10xxxxxx = 0x80
 * 00111111 = 0x3F
 *
 * UTF-8 codepage layout. Latin codepage needs 2 bytes to encode so other
 * bytes will be ignored and not checked. vCard format supports plain ASCII
 * by default, but other encodings can be specified. We will use UTF-8.
 * Since wchar type stores characters as Unicode code points they have
 * to be converted to UTF-8 encoded byte (e.g. U+010D to C4 8D). UTF-8
 * is a variable-width character encoding, as seen from table above.
 */
inline_func static int_u8_t
ucp_to_utf8(int_u8_t * restrict_ptr utf8, const wchar_t ucp)
{
	if (ucp <= 0x7F) {
		*utf8 = (int_u8_t) ucp;
		return 1;
	} else if (ucp <= 0x17F) {
		*utf8++ = (int_u8_t) (0xC0 | (ucp >> 6));
		*utf8 = (int_u8_t) (0x80 | (ucp & 0x3F));
		return 2;
	} else {
		exit(-EINVAL);
	}
}

/* Write converted UTF-8 byte in hex to file */
inline_func static void
write_utf8(const wchar_t * restrict_ptr const clan,
	FILE * restrict_ptr const vcf)
{
	int_u8_t convert[2] = { 0 };
	register int_u8_t len = 0;
	register size_t str_len = 0;
	register int_u32_t i = 0, j = 0;

	str_len = wcslen(clan);
	for (i = 0; i < str_len; ++i) {
		len = ucp_to_utf8(convert, *(clan + i));
		for (j = 0; j < len; ++j) fprintf(vcf, "=%X", *(convert + j));
	}
	return;
}
#endif /* HAS_WCHAR_H */

/*
 * Save contact information (first name, last name, phone
 * number and email) to a virtual contact (.vcf) file
 * which can be imported elsewhere (i.e. Android)
 */
void
export_vcf(
	struct Kontakt * restrict_ptr const * restrict_ptr ptr,
	const int_u32_t broj_kontakata)
{
	struct Kontakt * restrict_ptr const * restrict_ptr const
	endptr = ptr + broj_kontakata;
	FILE * restrict_ptr const vcard_fp = fopen(DATA_DIR "/export.vcf", "w");

	if (!vcard_fp || errno == ENOENT) {
		PRINT_ERRNO(ENOENT);
		PRINT_ERR(PR_STR, MSG_ERR_EXPORT);
		return;
	}

	for (; ptr < endptr; ++ptr) {
#ifdef HAS_WCHAR_H
		fprintf(vcard_fp,
		"BEGIN:VCARD\n"
		"VERSION:2.1\n"
		"N;CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:");

		write_utf8((*ptr)->prezime, vcard_fp);
		fprintf(vcard_fp, ";");

		write_utf8((*ptr)->ime, vcard_fp);
		fprintf(vcard_fp,
		";;;\n"
		"FN;CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:");

		write_utf8((*ptr)->ime, vcard_fp);
		fprintf(vcard_fp, "=20");

		write_utf8((*ptr)->prezime, vcard_fp);
		fprintf(vcard_fp,
		"\nTEL;CELL:%ls\n"
		"EMAIL;HOME:%ls\n"
		"END:VCARD\n\n",
		(*ptr)->broj, (*ptr)->email);
#else
		fprintf(vcard_fp,
		"BEGIN:VCARD\n"
		"VERSION:2.1\n"
		"N:%s;%s;;;\n"
		"FN:%s %s\n"
		"TEL;CELL:%s\n"
		"EMAIL;HOME:%s\n"
		"END:VCARD\n\n",
		(*ptr)->prezime, (*ptr)->ime, (*ptr)->ime,
		(*ptr)->prezime, (*ptr)->broj, (*ptr)->email);
#endif /* HAS_WCHAR_H */
	}
	fclose(vcard_fp);
	PRINT(PR_STR, MSG_EXPORT);
	return;
}
