/*
 *	ZavrsniProjekt
 *	Copyright (C) 2020  Denis Bošnjaković
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmakeconf.h>
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef HAS_WCHAR_H
#include <wchar.h>
#else
#include <string.h>
#endif /* HAS_WCHAR_H */

#include "definitions/define.h"
#include "definitions/inline.h"
#include "definitions/int.h"
#include "definitions/restrict.h"
#include "definitions/struct.h"
#include "definitions/wide.h"
#include "functions/input.h"
#include "functions/sort.h"
#include "functions/terminal.h"
#include "functions/std/strtoul.h"
#include "messages/print.h"
#include "messages/sort.h"

enum SortParameter {
	FNAME = 1,
	LNAME,
	PHONE,
	AGE,
	YEAR
};

enum SortMethod {
	ASCENDING = 1,
	DESCENDING
};

typedef int_u32_t (*compare_f)(
		struct Kontakt * restrict_ptr const * restrict_ptr const,
		struct Kontakt * restrict_ptr const * restrict_ptr const);

inline_func static void shellsort(struct Kontakt ** restrict_ptr const,
					const int_s64_t, compare_f);

/* Functions for shell sort comparison */
inline_func static int_u32_t
cmp_fname_asc(
	struct Kontakt * restrict_ptr const * restrict_ptr const s1,
	struct Kontakt * restrict_ptr const * restrict_ptr const s2)
{
	return (STRCOLL_DEF((*s1)->ime, (*s2)->ime) > 0);
}

inline_func static int_u32_t
cmp_fname_desc(
	struct Kontakt * restrict_ptr const * restrict_ptr const s1,
	struct Kontakt * restrict_ptr const * restrict_ptr const s2)
{
	return (STRCOLL_DEF((*s2)->ime, (*s1)->ime) > 0);
}

inline_func static int_u32_t
cmp_lname_asc(
	struct Kontakt * restrict_ptr const * restrict_ptr const s1,
	struct Kontakt * restrict_ptr const * restrict_ptr const s2)
{
	return (STRCOLL_DEF((*s1)->prezime, (*s2)->prezime) > 0);
}

inline_func static int_u32_t
cmp_lname_desc(
	struct Kontakt * restrict_ptr const * restrict_ptr const s1,
	struct Kontakt * restrict_ptr const * restrict_ptr const s2)
{
	return (STRCOLL_DEF((*s2)->prezime, (*s1)->prezime) > 0);
}

inline_func static int_u32_t
cmp_age_asc(
	struct Kontakt * restrict_ptr const * restrict_ptr const s1,
	struct Kontakt * restrict_ptr const * restrict_ptr const s2)
{
	return (str_uint((*s1)->godine, NULL) > str_uint((*s2)->godine, NULL));
}

inline_func static int_u32_t
cmp_age_desc(
	struct Kontakt * restrict_ptr const * restrict_ptr const s1,
	struct Kontakt * restrict_ptr const * restrict_ptr const s2)
{
	return (str_uint((*s2)->godine, NULL) > str_uint((*s1)->godine, NULL));
}

inline_func static int_u32_t
cmp_num_asc(
	struct Kontakt * restrict_ptr const * restrict_ptr const s1,
	struct Kontakt * restrict_ptr const * restrict_ptr const s2)
{
	return (str_uint((*s1)->broj, NULL) > str_uint((*s2)->broj, NULL));
}

inline_func static int_u32_t
cmp_num_desc(
	struct Kontakt * restrict_ptr const * restrict_ptr const s1,
	struct Kontakt * restrict_ptr const * restrict_ptr const s2)
{
	return (str_uint((*s2)->broj, NULL) > str_uint((*s1)->broj, NULL));
}

inline_func static int_u32_t
cmp_year_asc(
	struct Kontakt * restrict_ptr const * restrict_ptr const s1,
	struct Kontakt * restrict_ptr const * restrict_ptr const s2)
{
	return (*s1)->datum_rodjenja.gggg > (*s2)->datum_rodjenja.gggg;
}

inline_func static int_u32_t
cmp_year_desc(
	struct Kontakt * restrict_ptr const * restrict_ptr const s1,
	struct Kontakt * restrict_ptr const * restrict_ptr const s2)
{
	return (*s2)->datum_rodjenja.gggg > (*s1)->datum_rodjenja.gggg;
}

/*
 * Shell sort is an optimization of insertion sort
 * that allows the exchange of items far apart
 */
inline_func static void
shellsort(struct Kontakt ** restrict_ptr const polje,
		const int_s64_t elementi, compare_f cmp)
{
	register int_s64_t razmak = 0, i = 0, j = 0;
	register struct Kontakt *tmp = NULL;

	for (razmak = elementi >> 1; razmak > 0; --razmak) {
		for (i = razmak; i < elementi; ++i)
			for (j = i - razmak; j >= 0 && cmp((polje + j),
			(polje + (j + razmak))); j -= razmak) {
				tmp = *(polje + j);
				*(polje + j) = *(polje + (j + razmak));
				*(polje + (j + razmak)) = tmp;
			}
	}
	return;
}

/*
 * Sort first name, last name, phone number
 * and age in ascending or descending order
 */
void
sortiranje_kontakata(struct Kontakt ** restrict_ptr const polje,
					const int_u32_t elementi)
{
	int_u8_t parameter = 0, sort_method = 0;
	compare_f func_ptr = NULL;

	/* No point in sorting only 1 contact */
	if (elementi < 2) return;

	clear_terminal();
	PRINT(PR_STR, MSG_MENU_SORT);
	do {
		parameter = (int_u8_t) get_uint_input(3);
	} while(parameter < FNAME || parameter > YEAR);

	clear_terminal();
	PRINT(PR_STR, MSG_MENU_SORT_METHOD);
	do {
		sort_method = (int_u8_t) get_uint_input(3);
	} while(sort_method != ASCENDING && sort_method != DESCENDING);

	switch (parameter) {
	case FNAME:
		switch (sort_method) {
		case ASCENDING: func_ptr = cmp_fname_asc; break;
		case DESCENDING: func_ptr = cmp_fname_desc; break;
		default: exit(-EINVAL);
		} break;
	case LNAME:
		switch (sort_method) {
		case ASCENDING: func_ptr = cmp_lname_asc; break;
		case DESCENDING: func_ptr = cmp_lname_desc; break;
		default: exit(-EINVAL);
		} break;
	case PHONE:
		switch (sort_method) {
		case ASCENDING: func_ptr = cmp_num_asc; break;
		case DESCENDING: func_ptr = cmp_num_desc; break;
		default: exit(-EINVAL);
		} break;
	case AGE:
		switch (sort_method) {
		case ASCENDING: func_ptr = cmp_age_asc; break;
		case DESCENDING: func_ptr = cmp_age_desc; break;
		default: exit(-EINVAL);
		} break;
	case YEAR:
		switch (sort_method) {
		case ASCENDING: func_ptr = cmp_year_asc; break;
		case DESCENDING: func_ptr = cmp_year_desc; break;
		default: exit(-EINVAL);
		} break;
	default:
		PRINT_ERR(PR_STR, WIDE(MSG_WRONG_CHOICE));
		exit(-EINVAL);
	}

	shellsort(polje, (int_s64_t) elementi, func_ptr);
	return;
}
