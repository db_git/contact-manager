/*
 *	ZavrsniProjekt
 *	Copyright (C) 2020  Denis Bošnjaković
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmakeconf.h>
#include <stdio.h>

#ifdef HAS_WCHAR_H
#include <wchar.h>
#include <wctype.h>
#else
#include <ctype.h>
#include <string.h>
#endif /* HAS_WCHAR_H */

#include "definitions/bool.h"
#include "definitions/char.h"
#include "definitions/define.h"
#include "definitions/wide.h"
#include "functions/input.h"
#include "functions/terminal.h"

/* Simple yes or no answer */
int
answer(void)
{
	int odgovor = 0;
	bool_t wrong_answer = FALSE_DEF;
	char_t exit[4] = { '\0' };

	do {
		get_string_input(exit, 4);
		*exit = (char_t) TOLOWER_DEF((int_t) *exit);
		*(exit + 1) = (char_t) TOLOWER_DEF((int_t) *(exit + 1));

		if (!STRNCMP_DEF(exit, WIDE("da"), 2)) {
			odgovor = 1;
			wrong_answer = FALSE_DEF;
		} else if (!STRNCMP_DEF(exit, WIDE("ne"), 2)) {
			odgovor = -1;
			wrong_answer = FALSE_DEF;
		}
		else {
			wrong_answer = TRUE_DEF;
		}
	} while(wrong_answer);
	return odgovor;
}
