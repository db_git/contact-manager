/*
 *	ZavrsniProjekt
 *	Copyright (C) 2020  Denis Bošnjaković
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if defined(_MSC_VER)		\
 || defined(_MSC_FULL_VER)	\
 || defined(_MSC_BUILD)
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE
#endif /* _MSC_VER, _MSC_FULL_VER, _MSC_BUILD */

#include <cmakeconf.h>
#include <errno.h>
#include <locale.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef HAS_WCHAR_H
#include <wchar.h>
#ifdef _WIN32
#include <fcntl.h>
#include <io.h>
#endif /* _WIN32 */
#endif /* HAS_WCHAR_H */

#include "definitions/bool.h"
#include "definitions/int.h"
#include "definitions/struct.h"
#include "definitions/wide.h"
#include "functions/contact.h"
#include "functions/file.h"
#include "functions/input.h"
#include "functions/memory.h"
#include "functions/sort.h"
#include "functions/terminal.h"
#include "functions/vcard.h"
#include "messages/main.h"
#include "messages/print.h"

enum Izbornik {
	IZLAZ,
	DODAJ,
	UREDI,
	OBRISI,
	ISPIS,
	SORTIRAJ,
	IZVOZ,
	SPREMI
};

int
main(void)
{
	bool_t empty_file = FALSE_DEF;
	struct Kontakt **polje_kontakti = NULL;
	int_u32_t broj_kontakata = 0;
	int_u8_t opcija = 0;
	int odgovor = 0;

	check_empty_file(&empty_file);
	if (!empty_file) polje_kontakti = ucitaj_kontakte(&broj_kontakata);

#ifdef HAS_WCHAR_H
#ifdef _WIN32
	_setmode(_fileno(stdin), _O_U16TEXT);
	_setmode(_fileno(stdout), _O_U16TEXT);
	setlocale(LC_ALL, "hr-HR");
#else
	setlocale(LC_ALL, "hr_HR.UTF-8");
#endif /* _WIN32 */
#else
	setlocale(LC_ALL, "C");
#endif /* HAS_WCHAR_H */

	do {
		clear_terminal();
		PRINT(PR_STR, MSG_MENU_MAIN);
		do {
			opcija = (int_u8_t) get_uint_input(3);
		} while(opcija > SPREMI);

		clear_terminal();
		switch (opcija) {
		case IZLAZ:
			PRINT(PR_STR, MSG_EXIT);
			odgovor = answer();
			switch (odgovor) {
			case -1:
				opcija = 8;
				break;
			case 1:
				opcija = 0;
				if (polje_kontakti) {
					spremi_kontakte(polje_kontakti,
					broj_kontakata);
					struct_dealloc(&polje_kontakti,
					broj_kontakata);
				} else {
					remove(DATA_DIR "/kontakti.bin");
				}
				break;
			default:
				PRINT_ERR(PR_STR, WIDE(MSG_WRONG_CHOICE));
				exit(-EINVAL);
			}
			break;
		case DODAJ:
			dodaj_kontakt(&polje_kontakti, &broj_kontakata);
			spremi_kontakte(polje_kontakti, broj_kontakata);
			confirmation();
			break;
		case UREDI:
			if (polje_kontakti) {
				uredi_kontakt(polje_kontakti, broj_kontakata);
				spremi_kontakte(polje_kontakti, broj_kontakata);
			}
			else {
				PRINT(PR_STR, MSG_NO_EDIT);
			}

			confirmation();
			break;
		case OBRISI:
			if (polje_kontakti) {
				polje_kontakti = obrisi_kontakt(
				polje_kontakti, &broj_kontakata);
				PRINT(WIDE("\n"));
			} else {
				PRINT(PR_STR, WIDE(MSG_NO_DELETE));
			}

			if (polje_kontakti)
				spremi_kontakte(polje_kontakti, broj_kontakata);

			confirmation();
			break;
		case ISPIS:
			if (polje_kontakti)
				ispis_kontakata(polje_kontakti, broj_kontakata);
			else
				PRINT(PR_STR, WIDE(MSG_NO_PRINT));

			confirmation();
			break;
		case SORTIRAJ:
			if (polje_kontakti) {
				sortiranje_kontakata(polje_kontakti, broj_kontakata);
				spremi_kontakte(polje_kontakti, broj_kontakata);
				PRINT(PR_STR, WIDE(MSG_SORTED));
			} else {
				PRINT(PR_STR, WIDE(MSG_NO_SORT));
			}

			confirmation();
			opcija = 5;
			break;
		case IZVOZ:
			clear_terminal();
			if (polje_kontakti)
				export_vcf(polje_kontakti, broj_kontakata);
			else PRINT(PR_STR, WIDE(MSG_NO_EXPORT));

			confirmation();
			break;
		case SPREMI:
			clear_terminal();
			if (polje_kontakti) {
				spremi_kontakte(polje_kontakti, broj_kontakata);
				PRINT(PR_STR, WIDE(MSG_SAVED));
			} else {
				PRINT(PR_STR, WIDE(MSG_NO_SAVE));
			}

			confirmation();
			break;
		default:
			/* Not supposed to get here, abort */
			PRINT_ERR(PR_STR, WIDE(MSG_WRONG_CHOICE));
			exit(-EINVAL);
		}
	} while(opcija);
	exit(EXIT_SUCCESS);
}
