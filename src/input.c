/*
 *	ZavrsniProjekt
 *	Copyright (C) 2020  Denis Bošnjaković
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmakeconf.h>
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef HAS_WCHAR_H
#include <wchar.h>
#include <wctype.h>
#else
#include <ctype.h>
#include <string.h>
#endif /* HAS_WCHAR_H */

#include "definitions/bool.h"
#include "definitions/char.h"
#include "definitions/define.h"
#include "definitions/restrict.h"
#include "definitions/wide.h"
#include "functions/input.h"
#include "functions/memory.h"
#include "functions/terminal.h"
#include "functions/std/fgets.h"
#include "functions/std/strtoul.h"
#include "messages/input.h"
#include "messages/print.h"

static int isstrempty(const char_t * restrict_ptr);
static int chkintstr(const char_t * restrict_ptr);
static int chkfunnychars(const char_t * restrict_ptr);

/* Check for empty user input (no chars, only whitespace) */
static int
isstrempty(const char_t * restrict_ptr input)
{
	register size_t slen = STRLEN_DEF(input);;

	for (; slen--; ++input)
		if (!ISSPACE_DEF((int_t) *input)) return 0;

	return 1;
}

/* Check if number has been entered in string */
static int
chkintstr(const char_t * restrict_ptr input)
{
	register size_t slen = STRLEN_DEF(input);

	for (; slen--; ++input)
		if (ISDIGIT_DEF((int_t) *input)) return 1;

	return 0;
}

/* Check for non digit chars in input */
static int
chkfunnychars(const char_t * restrict_ptr input)
{
	register size_t slen = STRLEN_DEF(input);

	for (; slen--; ++input) {
		if (ISALPHA_DEF((int_t) *input) || ISPUNCT_DEF((int_t) *input))
			return 1;
	}
	return 0;
}

/* Get user input as string */
void
get_string_input(char_t * restrict_ptr const input,
			const int_u32_t num_of_chars)
{
	bool_t buffer_garbage = FALSE_DEF;

	do {
		if (!get_str(input, num_of_chars)) {
			PRINT_ERRNO(EIO);
			PRINT_ERR(PR_STR, WIDE(MSG_ERR_FGETS));
			exit(-EIO);
		} else if (!STRCHR_DEF(input, '\n')) {
			buffer_garbage = TRUE_DEF;
			CLEAR_BUFFER();
		} else {
			buffer_garbage = FALSE_DEF;
			*(input + STRCSPN_DEF(input, WIDE("\n"))) = '\0';
		}
	} while(buffer_garbage || isstrempty(input));
	return;
}

/* Get input as a string, convert to unsigned number */
int_u32_t
get_uint_input(const int_u32_t num_digits)
{
	bool_t buffer_garbage = FALSE_DEF;
	int_u32_t trazeni_broj = 0;
	char_t *input = NULL, *rem = NULL;

	input = scalloc(num_digits, sizeof *input);
	do {
		if(!get_str(input, num_digits)) {
			PRINT_ERRNO(EIO);
			PRINT_ERR(PR_STR, WIDE(MSG_ERR_FGETS));
			exit(-EIO);
		} else if (!STRCHR_DEF(input, '\n')) {
			buffer_garbage = TRUE_DEF;
			CLEAR_BUFFER();
		} else {
			buffer_garbage = FALSE_DEF;
		}
	} while(buffer_garbage || isstrempty(input) || *input == '-'
	|| !chkintstr(input) || chkfunnychars(input));

	trazeni_broj = (int_u32_t) str_uint(input, &rem);
	if (!rem || errno == ERANGE) {
		PRINT_ERRNO(ERANGE);
		PRINT_ERR(PR_STR, WIDE(MSG_ERR_STRTOUL));
		exit(-ERANGE);
	}

	sfree((void *) &input, num_digits, sizeof *input);
	return trazeni_broj;
}
