#ifndef DEFINITIONS_RESTRICT_H
#define DEFINITIONS_RESTRICT_H 1

/* While not necessary, restricting pointer aliasing can improve optimization */
#if defined(__clang__) || defined(__CLANG__) || defined(__GNUC__) || defined(__TINYC__)
	#define restrict_ptr __restrict__

#elif defined(_MSC_VER) || defined(_MSC_FULL_VER) || defined(_MSC_BUILD)
	#define restrict_ptr __restrict

#elif defined(__STDC_VERSION__) && ((__STDC_VERSION__) >= (199901L))
	#define restrict_ptr restrict

#else
	#define restrict_ptr

#endif /* __clang__, __CLANG__, __GNUC__, _MSC_VER,
	_MSC_FULL_VER, _MSC_BUILD, __STDC_VERSION__ */

#endif /* DEFINITIONS_RESTRICT_H */
