#ifndef DEFINITIONS_CHAR_H
#define DEFINITIONS_CHAR_H 1

#ifdef HAS_WCHAR_H

#include <wchar.h>

typedef wchar_t char_t;
typedef wint_t int_t;

#else

typedef char char_t;
typedef int int_t;

#endif /* HAS_WCHAR_H */

#endif /* DEFINITIONS_CHAR_H */
