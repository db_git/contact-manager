#ifndef DEFINITIONS_STRUCT_H
#define DEFINITIONS_STRUCT_H 1

#include "definitions/char.h"
#include "definitions/int.h"

enum StructMemberLength {
	FNAME_LENGTH	=	42,
	LNAME_LENGTH	=	61,
	PHONE_LENGTH	=	15,
	EMAIL_LENGTH	=	130,
	AGE_LENGTH	=	5,
	GENDER_LENGTH	=	3,
	DATE_LENGTH	=	12
};

struct Datum {
	int_u16_t mm;
	int_u16_t dd;
	int_u16_t gggg;
};

struct Kontakt {
	struct Datum datum_rodjenja;
	char_t email[EMAIL_LENGTH];
	char_t prezime[LNAME_LENGTH];
	char_t ime[FNAME_LENGTH];
	char_t broj[PHONE_LENGTH];
	char_t godine[AGE_LENGTH];
	char_t spol[GENDER_LENGTH];
};

#endif /* DEFINITIONS_STRUCT_H */
