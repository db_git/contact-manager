#ifndef DEFINITIONS_DEFINE_H
#define DEFINITIONS_DEFINE_H

#ifdef HAS_WCHAR_H

#define EOF_DEF WEOF
#define GETCHAR_DEF getwchar

#define ISALNUM_DEF iswalnum
#define ISALPHA_DEF iswalpha
#define ISDIGIT_DEF iswdigit
#define ISPUNCT_DEF iswpunct
#define ISSPACE_DEF iswspace

#define STRCHR_DEF wcschr
#define STRCOLL_DEF wcscoll
#define STRCSPN_DEF wcscspn
#define STRLEN_DEF wcslen
#define STRSPN_DEF wcsspn
#define STRNCMP_DEF wcsncmp
#define STRNCPY_DEF wcsncpy
#define SPRINTF_DEF swprintf

#define TOLOWER_DEF towlower
#define TOUPPER_DEF towupper

#else

#define EOF_DEF EOF
#define GETCHAR_DEF getchar

#define ISALNUM_DEF isalnum
#define ISALPHA_DEF isalpha
#define ISDIGIT_DEF isdigit
#define ISPUNCT_DEF ispunct
#define ISSPACE_DEF isspace

#define STRCHR_DEF strchr
#define STRCOLL_DEF strcoll
#define STRCSPN_DEF strcspn
#define STRLEN_DEF strlen
#define STRSPN_DEF strspn
#define STRNCMP_DEF strncmp
#define STRNCPY_DEF strncpy
#define SPRINTF_DEF snprintf

#define TOLOWER_DEF tolower
#define TOUPPER_DEF toupper
#endif /* HAS_WCHAR_H */

#endif /* DEFINITIONS_DEFINE_H */
