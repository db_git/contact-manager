#ifndef DEFINITIONS_OS_CHECK
#define DEFINITIONS_OS_CHECK 1

#include <limits.h>

#if (ULONG_MAX == UINT_MAX)
#define OS_TYPE 32
#elif (ULONG_MAX > UINT_MAX)
#define OS_TYPE 64
#else
#error "Failed to determine if OS is x32 or x64"
#endif /* ULONG_MAX, UINT_MAX */

#endif /* DEFINITIONS_OS_CHECK */
