#ifndef MESSAGES_MEMORY_H
#define MESSAGES_MEMORY_H 1

#include "definitions/int.h"

#define MSG_ERR_REALLOC "\nerror: realloc() failed\n"

#ifdef HAS_WCHAR_H
#define MSG_ERR_CALLOC PR_U32 L" elements with size of " PR_U32 L"\n"
#else
#define MSG_ERR_CALLOC PR_U32 " elements with size of " PR_U32 "\n"
#endif /* HAS_WCHAR_H */

#endif /* MESSAGES_MEMORY_H */
