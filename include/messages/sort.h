#ifndef MESSAGES_SORT_H
#define MESSAGES_SORT_H 1

#define MSG_WRONG_CHOICE "Krivi odabir!\n"

#ifdef HAS_WCHAR_H
#define MSG_MENU_SORT L"Kako " L"\u017e" L"elite "		\
			L"sortirati kontakte?\n\n"		\
			L"1) Prema imenu\n"			\
			L"2) Prema prezimenu\n"			\
			L"3) Prema broju mobitela\n"		\
			L"4) Prema dobi\n"			\
			L"5) Prema godini ro"			\
			L"\u0111" L"enja\n\n"

#define MSG_MENU_SORT_METHOD L"\u017d" L"elite li sortirati"	\
				L" uzlazno ili silazno?\n\n"	\
				L"1) Uzlazno\n"			\
				L"2) Silazno\n\n"
#else
#define MSG_MENU_SORT "Kako zelite sortirati kontakte?\n\n"	\
			"1) Prema imenu\n"			\
			"2) Prema prezimenu\n"			\
			"3) Prema broju mobitela\n"		\
			"4) Prema dobi\n"			\
			"5) Prema godini rodenja\n\n"

#define MSG_MENU_SORT_METHOD "Zelite li sortirati "		\
				"uzlazno ili silazno?\n\n"	\
				"1) Uzlazno\n"			\
				"2) Silazno\n\n"
#endif /* HAS_WCHAR_H */

#endif /* MESSAGES_SORT_H */
