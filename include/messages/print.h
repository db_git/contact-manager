#ifndef MESSAGES_PRINT_H
#define MESSAGES_PRINT_H 1

#include <errno.h>

#include "definitions/wide.h"

#ifdef HAS_WCHAR_H
#include <wchar.h>
#include <stdio.h>

/*
 * Because MSVC doesn't actually define __STDC_VERSION__
 * macro and has partial C99 standard support (more like
 * broken than partial) it needs separate checking
 */
#if (defined(_MSC_VER)				\
 || defined(_MSC_FULL_VER)			\
 || defined(_MSC_BUILD))			\
 || (defined(__STDC_VERSION__)			\
 && (__STDC_VERSION__ >= 199901L))

#define PR_STR L"%ls"
#define PRINT(...) fwprintf(stdout, __VA_ARGS__)
#define PRINT_ERR(...) fwprintf(stderr, __VA_ARGS__)

#else
#error "Minimum standard required for variadic macros is C99"
#endif /* __STDC_VERSION__ */

#else
#include <stdio.h>

#if (defined(_MSC_VER)				\
 || defined(_MSC_FULL_VER)			\
 || defined(_MSC_BUILD))			\
 || (defined(__STDC_VERSION__)			\
 && (__STDC_VERSION__ >= 199901L))

#define PR_STR "%s"
#define PRINT(...) fprintf(stdout, __VA_ARGS__)
#define PRINT_ERR(...) fprintf(stderr, __VA_ARGS__)

#else
#error "Minimum standard required for variadic macros is C99"
#endif /* __STDC_VERSION__ */

#endif /* HAS_WCHAR_H */

#define PRINT_ERRNO(__err_code__)					\
	do {								\
		errno = (__err_code__);					\
		perror("\nERROR");					\
		PRINT_ERR(PR_STR WIDE(":%d\n"),			\
		WIDE(__FILE__), __LINE__);				\
		errno = 0;						\
	} while(0)

#endif /* MESSAGES_PRINT_H */
