#ifndef FUNCTIONS_TERMINAL_H
#define FUNCTIONS_TERMINAL_H 1

#include <stdio.h>

#if !(defined(__unix__)		\
 || defined(__linux__)		\
 || defined(__gnu_linux__)	\
 || (defined(__APPLE__)		\
 && defined(__MACH__)))
#include <stdlib.h>
#else
#define CLEAR_SCREEN "\x1b\x5b\x48\x1b\x5b\x32\x4a\x1b\x5b\x33\x4a"
#endif /* __unix__, __linux__, __gnu_linux__, __APPLE__, __MACH__ */

#include "definitions/char.h"
#include "definitions/define.h"
#include "definitions/inline.h"
#include "definitions/wide.h"
#include "messages/print.h"
#include "messages/terminal.h"

/* Clear excess characters left in buffer */
#define CLEAR_BUFFER() for (int_t c = 0; c != '\n' && c != EOF_DEF; c = GETCHAR_DEF())

inline_func static void clear_terminal(void);
inline_func static void confirmation(void);

int answer(void);

/* Clear terminal screen */
inline_func static void
clear_terminal(void)
{
#if defined(_WIN32)
	system("cls");
#elif defined(__unix__)			\
 || defined(__linux__)			\
 || defined(__gnu_linux__)		\
 || (defined(__APPLE__)			\
 && defined(__MACH__))
	PRINT(PR_STR, WIDE(CLEAR_SCREEN));
#else
	system("cls||clear");
/* else: Throw the monitor out the window */
#endif /* _WIN32, __unix__, __linux__, __gnu_linux__, __APPLE__, __MACH__ */
	return;
}

/* Wait for Enter to be pressed */
inline_func static void
confirmation(void)
{
	PRINT(PR_STR, WIDE(MSG_ENTER));
	CLEAR_BUFFER();
	return;
}

#endif /* FUNCTIONS_TERMINAL_H */
