#ifndef FUNCTIONS_CONTACT_H
#define FUNCTIONS_CONTACT_H 1

#include "definitions/int.h"
#include "definitions/restrict.h"
#include "definitions/struct.h"

void dodaj_kontakt(struct Kontakt ** restrict_ptr * restrict_ptr const,
				int_u32_t * restrict_ptr const);

void ispis_kontakata(struct Kontakt * restrict_ptr const * restrict_ptr const,
							const int_u32_t);

void uredi_kontakt(struct Kontakt * restrict_ptr const * restrict_ptr const,
							const int_u32_t);

struct Kontakt **obrisi_kontakt(struct Kontakt ** restrict_ptr,
				int_u32_t * restrict_ptr const);

#endif /* FUNCTIONS_CONTACT_H */
