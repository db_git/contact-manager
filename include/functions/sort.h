#ifndef FUNCTIONS_SORT_H
#define FUNCTIONS_SORT_H 1

#include "definitions/int.h"
#include "definitions/restrict.h"
#include "definitions/struct.h"

void sortiranje_kontakata(struct Kontakt ** restrict_ptr const, const int_u32_t);

#endif /* FUNCTIONS_SORT_H */
