#ifndef FUNCTIONS_MEMORY_H
#define FUNCTIONS_MEMORY_H 1

#include "definitions/inline.h"
#include "definitions/int.h"
#include "definitions/restrict.h"
#include "definitions/struct.h"

inline_func static void memzero(void * restrict_ptr const,
				const int_u32_t, const int_u32_t);

inline_func static struct Kontakt ** struct_alloc(const int_u32_t);
inline_func static void struct_dealloc(struct Kontakt *** restrict_ptr const,
							const int_u32_t);

void *scalloc(const int_u32_t, const int_u32_t);
void sfree(void * restrict_ptr * restrict_ptr, const int_u32_t, const int_u32_t);
void *srealloc(void * const, const int_u32_t, const int_u32_t, const int_u32_t);

/* Initialise memory to zero */
inline_func static void
memzero(void * restrict_ptr const ptr,
	const int_u32_t elements, const int_u32_t size)
{
#if defined(_MSC_VER)			\
 || defined(_MSC_FULL_VER)		\
 || defined(_MSC_BUILD)
	volatile char *ptp = (volatile char *) ptr;
	volatile char *endptr = ((volatile char *) ptp + (elements*size));
#else
	volatile char * volatile ptp = (volatile char * volatile) ptr;
	volatile char * volatile endptr = ((volatile char * volatile)
	ptp + (elements*size));
#endif /* _MSC_VER, _MSC_FULL_VER, _MSC_BUILD */
	for (; ptp < endptr; *ptp = 0UL, ++ptp);
	return;
}

/* Allocate memory for array of struct pointers */
inline_func static struct Kontakt **
struct_alloc(const int_u32_t broj)
{
	register int_u32_t i = 0;
	struct Kontakt ** restrict_ptr polje = scalloc(broj, sizeof *polje);

	for (i = 0; i < broj; ++i)
		*(polje + i) = scalloc(1, sizeof **polje);

	return polje;
}

/* Free struct array */
inline_func static void
struct_dealloc(
	struct Kontakt *** restrict_ptr const polje,
	const int_u32_t broj)
{
	register int_u32_t i = 0;

	for (i = 0; i < broj; ++i)
		sfree((void *) (*polje + i), 1, sizeof ***polje);
	sfree((void *) polje, broj, sizeof **polje);

	return;
}

#endif /* FUNCTIONS_MEMORY_H */
