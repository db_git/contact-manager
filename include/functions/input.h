#ifndef FUNCTIONS_INPUT_H
#define FUNCTIONS_INPUT_H 1

#include "definitions/char.h"
#include "definitions/int.h"
#include "definitions/restrict.h"

void get_string_input(char_t * restrict_ptr const, const int_u32_t);
int_u32_t get_uint_input(const int_u32_t);

#endif /* FUNCTIONS_INPUT_H */
