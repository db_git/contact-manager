#ifndef LIBFUNC_FGETS_H
#define LIBFUNC_FGETS_H

#include "definitions/char.h"
#include "definitions/int.h"

char_t *get_str(register char_t *, register int_u32_t);

#endif /* LIBFUNC_FGETS_H */
