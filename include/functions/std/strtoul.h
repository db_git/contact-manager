#ifndef LIBFUNC_STRTOUL_H
#define LIBFUNC_STRTOUL_H

#include "definitions/char.h"
#include "definitions/int.h"
#include "definitions/restrict.h"

int_u64_t str_uint(char_t * restrict_ptr const,
		char_t * restrict_ptr * restrict_ptr const);

#endif /* LIBFUNC_STRTOUL_H */
